import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  template: `
    <h2>Welcome {{ name }}</h2>
    <h2 class="text-success">Codevolution</h2>
    <h2 [class]="successClass">Codevolution</h2>
    <!--can use both together: -->
    <h2 class="text-special" [class]="successClass">Codevolution</h2>
    <!-- conditionally apply class (text-danger)  -->
    <h2 [class.text-danger]="hasError">Codevolution</h2>
    <!-- conditionally apply multiple classes, with Angular's ngClass directive  (a directive is a custom html attribute). to this you bind an object (messageclasses) Angular identifies that messageclasses has to be applied to this elemnt. It checks which are true, those that are true will be applied. Useful as it allows you to dynamically add/remove classes to certain html elemnts based on certain iteractions/state of application. -->
    <h2 [ngClass]="messageClasses">Codevolution</h2>
    <!-- style binding -->
    <h2 [style.color]="'orange'">Style Binding</h2>
    <h2 [style.color]="hasError ? 'red' : 'green'">Style Binding</h2>
    <h2 [style.color]="highlightColor">Style Binding 2</h2>
    <h2 [ngStyle]="titleStyles">Style Binding 3</h2>
    <button (click)="onClick()">Greet</button>
    <button (click)="greeting = 'Welcome Christelle'">Greet</button>
    {{ greeting }}
    <!-- Template reference variables: What we want is that when the user clicks on the button we should log to the console whatever is the text in the input element, we need to bind to the click event on the button: -->
    <input #myInput type="text" />
    <button (click)="logMessage(myInput.value)">Log</button>
    <!-- two way binding, view and model should always be in sync. [] property binding - data flow from the property to the template & () event binding, data flow from the template to the class -->
    <input [(ngModel)]="name" type="text" />
    {{ name }}
    <!-- we are binding to the id property of this input element -->
    <!-- <input [id]="myId" type="text" value="Christelle" /> -->
    <!-- can also do it with interpolation however there is a limit to this, can't do it for boolean values -->
    <!-- <input id="{{ myId }}" type="text" value="Christelle" /> -->
    <!-- for eg the disabled attribute of an input element, by default it is always set to false, if we try to set disabled to false to put it back to true (and therefore not disbaled) it doesn't work: -->
    <!-- <input disabled="false" id="{{ myId }}" type="text" value="Christelle" /> -->
    <!-- the solution is to use property binding: -->
    <!-- <input [disabled]="true" id="{{ myId }}" type="text" value="Christelle" /> -->
    <!-- we now create a new property, isdisabled in the class and then bind it -->
    <!-- <input
      [disabled]="isDisabled"
      id="{{ myId }}"
      type="text"
      value="Christelle"
    /> -->
  `,
  // <h2>Welcome {{ name }}</h2>
  //   <h2>{{ 2 + 1 }}</h2>
  //   <h2>{{ 'Welcome ' + name }}</h2>
  //   <h2>{{ name.length }}</h2>
  //   <h2>{{ name.toUpperCase() }}</h2>
  //   <h2>{{ greetUser() }}</h2>
  //   <!-- // you can not assign the result of an expression to a variable within duble curly braces: -->
  //   <!-- <h2>{{ a = 2+ 2}}</h2> -->
  //   <!-- the template is not aware of the global JS variables, so you cannot do this:  -->
  //   <!-- <h2>{{ window.location.href }}</h2> -->
  //   <h2>{{ siteUrl }}</h2>
  // `,
  // styleUrls: ['./test.component.css'],

  //creating new classes:
  // styles: [
  //   `
  //     .text-success {
  //       color: green;
  //     }
  //     .text-danger {
  //       color: red;
  //     }
  //     .text-special {
  //       font-style: italic;
  //     }
  //   `,
  // ],
  // style binding
  styles: [],
})

//create a new property in the class so that name can be dynamic, this is known as interpolation. Binds data from a class to the template.
export class TestComponent {
  // public name = 'Christelle';
  public greeting = '';
  public name = '';
  public successClass = 'text-success';
  public hasError = true;
  public isSpecial = true;
  public highlightColor = 'purple';
  public titleStyles = {
    color: 'blue',
    fontStyle: 'italic',
  };
  public messageClasses = {
    'text-success': !this.hasError,
    'text-danger': this.hasError,
    'text-special': this.isSpecial,
  };
  onClick() {
    console.log('Welcome to Codevolution');
    this.greeting = 'Welcome to Codevolution';
  }

  logMessage(value: any) {
    console.log(value);
  }
  // you can do this instead though, in the class and then bind it to the template:
  // public siteUrl = window.location.href;
  // new property, we need to bind this value to the html id property.
  // public myId = 'testId';
  // new property:
  // public isDisabled = true;
  // new method in component class, call it above in the inline template and the result will be rendered in the browser.
  // greetUser() {
  //   return 'Hello ' + this.name;
  // }
}
